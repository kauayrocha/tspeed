from flask import Flask
import sys, os
import signal
app = Flask(__name__)


@app.route('/tecnospeed',methods=['GET'])
def helloTecnospeed():
    return 'Hello TecnoSpeed'

@app.route('/tecnospeed/signalterm',methods=['GET','POST'])
def stopTecnospeed():
    print(signal.SIGINT)
    #raise(signal.SIGINT,EnvironmentError)
    #exit(signal.SIGINT)

    #os._exit(0)
    
#    print('teste')

if __name__ == '__main__':
    app.run()